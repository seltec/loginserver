using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace SeltecOidServer.Data
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Guid MasterOrganizationId { get; set; }
        public MasterOrganization MasterOrganization { get; set; }
        
        [MaxLength(2), MinLength(2)]
        [StringLength(2)]
        public string LanguageIsoCode { get; set; }
        public Language Language { get; set; }
    }
}
