﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeltecOidServer.Data
{
    public class Language
    {
        [Key]
        [MaxLength(2), MinLength(2)]
        [StringLength(2)]
        public string IsoCode { get; set; }

        public string Name { get; set; }
    }
}
