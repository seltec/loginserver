using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;
using NLog.Web;

namespace SeltecOidServer
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");
                CreateHostBuilder(args)
                    .Build()
                    .Run();
            }
            catch (Exception e)
            {
                //NLog: catch setup errors
                logger.Fatal(e, "Critical Stopped Seltec OID Login Server");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(log =>
                {
                    log.ClearProviders();

                    if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development)
                    {
                        log.AddConsole();
                        log.AddDebug();
                    }

                    log.AddEventLog(new EventLogSettings()
                    {
                        LogName = "Seltec OID Login",
                        SourceName = "Seltec OID Login"
                    });
                })
                .UseNLog()
                .ConfigureWebHostDefaults(options => options.UseStartup<Startup>());
    }
}
